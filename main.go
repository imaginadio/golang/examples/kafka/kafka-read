package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	kafka "github.com/segmentio/kafka-go"
	record "gitlab.com/imaginadio/golang/examples/kafka/kafka-record"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s TOPIC\n", os.Args[0])
		return
	}

	var (
		partition int
		topic     = os.Args[1]
	)

	fmt.Println("Read from topic:", topic)
	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:   []string{"localhost:9092"},
		Topic:     topic,
		Partition: partition,
		MinBytes:  10e3,
		MaxBytes:  10e6,
	})
	defer r.Close()

	if err := r.SetOffset(0); err != nil {
		panic(err)
	}

	ctx := context.Background()
	for {
		message, err := r.ReadMessage(ctx)
		if err != nil {
			fmt.Println(err)
			break
		}

		fmt.Printf("message at offset %d: %s = %s\n", message.Offset, string(message.Key), string(message.Value))
		var rec record.Record
		if err = json.Unmarshal(message.Value, &rec); err != nil {
			fmt.Println(err)
			continue
		}
		fmt.Printf("%T %v\n", rec, rec)
	}

}
